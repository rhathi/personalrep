public class PODTransactionParser implements FileParser { 
    public String fileExtType {get; set;}
    public POD[] pods {get; set;}
    
    public PODTransactionParser() {
        pods = new POD[] {}; 
    }
    
    public PODTransactionParser(String fileExt) {
        pods = new POD[] {}; 
        fileExtType = fileExt;
    }


    public List<Object> parseFile(Blob blobContent) {
        String stringContent = blobContent.toString();
         
        POD pod = new POD();
        String[] row = stringContent.split('\n');
        
        Integer counter = 0;
        String recordType = '';
        while (counter < row.size()) {
            
            if(fileExtType.equals('txt') ){
                parseTextFileForPOD(row[counter]);
                counter++;
            }else if(fileExtType.equals('csv')){
                //for csv file
                parseCSVFileForPOD(row[counter],counter);
                counter++;
            }
        }
        
        
        return pods;
    }
    
    private void parseTextFileForPOD( String bodyString ) {
        POD podDetail = new POD();
        String[] rowValues = bodyString.split('\\|');
        if(rowValues.size() == 1){
            return;
        }
        podDetail.AWB = rowValues[0];
        podDetail.UID = rowValues[1];
        podDetail.pickUpDate = getPHPDateFormat(rowValues[2]);
        podDetail.deliveryDate = getPHPDateFormat(rowValues[3]);
        podDetail.deliveredTo = rowValues[4]; 
        podDetail.deliveryStatus = matchDeliveryStatusForPickUp(rowValues[5]);
        podDetail.deliveryStatusFromFile = rowValues[5];
        pods.add(podDetail); 
    }
    
    private DateTime getPHPDateFormat(String dateValue){//15-MAR-11
        String[] dateArray = parseStringToDate(dateValue);

        return DateTime.newInstance(Integer.valueOf(dateArray[2]), getMonthFromFormatMMM(dateArray[1]), Integer.valueOf(dateArray[0]) ); //year,month,day
    }

    private String[] parseStringToDate(String source) {//format is 15-MAR-11 for text file from phillipines
        String[] dateArray = new String[3];
        
        String[] sources = source.split('-');//15-MAR-11
        dateArray[0] = sources[0];
        dateArray[1] = sources[1];
        if(sources[2].length() == 2){
            dateArray[2] = '20'+ sources[2];
        }else{
            dateArray[2] = sources[2];
        }
        return dateArray;
    }
    
    private void parseCSVFileForPOD(String contents, Integer lineNo) {  //ROWCONTENT
        System.debug('RHATHI CHKA '+contents);
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');   
        System.debug('RHATHI CHKB '+contents);
        contents = contents.replaceAll('""','DBLQT');  
        System.debug('RHATHI CHKC '+contents);
        List<String> lines = new List<String>();  
    
        // check for blank CSV lines (only commas) ,these rows may exist as last few rows in the end
          
        if ( contents.replaceAll(',','').trim().length() == 0) {
            return;//dont add in pods
        }
            
        List<String> cleanFields = new List<String>();  
        String compositeField;  
        Boolean makeCompositeField = false;  
        List<String> fields = contents.split(',');      
        
        for(String field : fields) {  
            if( fields[0].trim().length() == 0){
                break;//this line just have commas//just check if it comes here
            }
            if (field.startsWith('"') && field.endsWith('"')) {  
    
                cleanFields.add(field.replaceAll('DBLQT','""'));  //'""'
    
            } else if (field.startsWith('"')) {  
    
                makeCompositeField = true;  
    
                compositeField = field;  
    
            } else if (field.endsWith('"')) {  
    
                compositeField += ',' + field;  
    
                cleanFields.add(compositeField.replaceAll('DBLQT','""'));  //'""'
    
                makeCompositeField = false;  
    
            } else if (makeCompositeField) {  
    
                compositeField +=  ',' + field;  
    
            } else {  
    
                cleanFields.add(field.replaceAll('DBLQT','""'));  
    
            }  
    
        }
    
        POD podDetail = new POD();
        if(cleanFields.size() == 6){//FOR PHP
            podDetail.AWB = cleanFields.get(0);
            podDetail.UID = cleanFields.get(1);
            podDetail.pickUpDate = getPHPDateFormat(cleanFields.get(2));
            podDetail.deliveryDate = getPHPDateFormat(cleanFields.get(3));
            podDetail.deliveredTo = cleanFields.get(4);
            podDetail.deliveryStatus = matchDeliveryStatusForPickUp(cleanFields.get(5));
            podDetail.deliveryStatusFromFile = cleanFields.get(5); 
        }else{//FOR MIS
            if(lineNo == 0){
                return;
            }
            podDetail.AWB = cleanFields.get(0);
            podDetail.pickUpDate = getCsvFileDateFormat(cleanFields.get(2));
            podDetail.UID = cleanFields.get(8);
            podDetail.deliveryDate = getCsvFileDateFormat(cleanFields.get(19));
            podDetail.deliveredTo = cleanFields.get(20);
            podDetail.deliveryStatus = matchDeliveryStatusForPickUp(cleanFields.get(21));
            podDetail.deliveryStatusFromFile = cleanFields.get(21);
            
        }
        
        pods.add(podDetail);

}

    private DateTime getCsvFileDateFormat(String dtvalue){//10/18/2010 15:22   ...mm/dd/yyyy  reqd is yyyy-mm-dd //
        
        String[] dateTimeArray = dtvalue.trim().split(' ');
        String[] dateArray = dateTimeArray[0].split('/'); 
        
        if(dateArray[1].equalsIgnoreCase('0')){
            return null;
        }
        
        if(dateTimeArray.size() == 2){
            String[] timeArray = dateTimeArray[1].split(':');
            String strTime = dateTimeArray[1];
            if(timeArray.size() == 2){//only this condition is actually required
                strTime += ':00';
            }else if(timeArray.size() == 1){//to on the safer side..just put this also..
                strTime += ':00:00';
            }else if(timeArray.size() == 0){//...
                strTime += '00:00:00'; 
            }
            
            return Datetime.valueOf(Datetime.valueOf( dateArray[2] + '-' + dateArray[0] + '-' + dateArray[1]+   ' ' + strTime ) );
        }
        
        return DateTime.newInstance(Integer.valueOf(dateArray[2]), Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]) );//year,month,day
        
    }


    private String matchDeliveryStatusForPickUp(String status ){
        String deliveryStatus = status.trim(); 
        
        if(deliveryStatus.equalsIgnoreCase('DELIVERED') || deliveryStatus.equalsIgnoreCase('POD') || deliveryStatus.equalsIgnoreCase('Shipment delivered') ){
            return 'Picked Up';
        }else{
            return deliveryStatus;  
        }
    }

    public class POD {
        public String AWB{get; set;}
        public String UID{get; set;}
        public Datetime pickUpDate{get; set;}
        public Datetime deliveryDate{get; set;}
        public String deliveredTo{get; set;}
        public String deliveryStatus{get; set;}
        public String deliveryStatusFromFile{get; set;}
        
        public POD() {}
    }

    public String[] updateExtractedData() {
/*        List<Document_Tracker__c> docTracks = new List<Document_Tracker__c> ();
        List<Case__c> errorCases = new List<Case__c> ();
        List<String> uidList = new List<String>();
        
        Map<String, POD> uidMap = new Map<String, POD> ();
        Map<String, POD> uidRemovedMap = new Map<String, POD> ();
        
        for (POD pod : pods) {
            if(pod.UID == null || pod.UID.trim().length() == 0){
                createCase(errorCases, pod, 'Null value found for UID in upload file', null);
                continue; 
            }
            uidMap.put(pod.UID, pod);
            uidList.add(pod.UID); 
        }
        uidRemovedMap = uidMap.clone();
        
        try{
            List<Contact> contacts = [SELECT Id, UID__c FROM Contact WHERE UID__c IN :uidList];
            Pod pod = null;
            
            for (Contact contact : contacts) {
                
                pod = uidMap.get(contact.UID__c);
                
                try{    
                    if(pod.deliveryStatus.equals('Picked Up')){
                        
                       docTracks.add(new Document_Tracker__c(AWB__c = pod.AWB, UID__c = pod.UID, 
                                                              Pick_Up_Date__c = pod.pickUpDate, Delivery_Date__c = pod.deliveryDate,
                                                              Delivered_To__c = pod.deliveredTo, 
                                                              Barcode__c = pod.UID,
                                                              Contact__c = contact.Id, Status__c='Picked Up'));
                    }else{
                        
                        throw new PODException('Status of POD document is not \'Picked Up\'');
                        
                    }
                }catch(PODException npe){
                    
                    createCase(errorCases, pod, npe.getmessage(), contact.Id);
                }
                uidRemovedMap.remove(contact.UID__c);
                
            }
            System.debug('map size'+uidRemovedMap.size());
            for (String uid : uidRemovedMap.keySet()){
                pod = uidRemovedMap.get(uid);
                
                createCase(errorCases, pod, 'UID not found in Contact table.', null);
            }
            
            insert docTracks;
            insert errorCases;
        }catch(Queryexception qe){
            System.debug('Caught in QueryException in PODTransactionParser'+ qe.getMessage());
            return new String[] {'true', 'The document was not successfully loaded.'};
        }catch(Exception e){
            System.debug('Caught in Exception in PODTransactionParser'+ e.getMessage());
            return new String[] {'true', 'The document was not successfully loaded...'};
        }
  */      
        return new String[] {'true', 'The document has been successfully loaded.'};
    }
    

    
    private Integer getMonthFromFormatMMM(String value){//put in util class
        
        if(value.equalsIgnoreCase('JAN')){
            return 1;
        }else if(value.equalsIgnoreCase('FEB')){
            return 2;
        }else if(value.equalsIgnoreCase('MAR')){
            return 3;
        }else if(value.equalsIgnoreCase('APR')){
            return 4;
        }else if(value.equalsIgnoreCase('MAY')){
            return 5;
        }else if(value.equalsIgnoreCase('JUN')){
            return 6;
        }else if(value.equalsIgnoreCase('JUL')){
            return 7;
        }else if(value.equalsIgnoreCase('AUG')){
            return 8;
        }else if(value.equalsIgnoreCase('SEP')){
            return 9;
        }else if(value.equalsIgnoreCase('OCT')){
            return 10;
        }else if(value.equalsIgnoreCase('NOV')){
            return 11;
        }else if(value.equalsIgnoreCase('DEC')){
            return 12;
        }else{ 
            return -1;
        }   
    }
    /*
    private void createCase(List<Case__c> errorCases, POD pod, String reason, String cId){
        Case__c errorCase = new Case__c();
        errorCase.Case_Reason__c = reason;
        errorCase.Description__c = 'Our document details : ' +
        '  AWB is : ' + pod.AWB +
        '  UID is : ' + pod.UID +
        '  Pick Up Date is : ' + pod.pickUpDate +
        '  Delivery Date is : ' + pod.deliveryDate +
        '  Delivered To is : ' + pod.deliveredTo +
        '  Status is : ' + pod.deliveryStatusFromFile;  
        
        if(cId !=  null){
            errorCase.Description__c += 'Contact Name is : ' + cId;
        }                   
                                    
        errorCases.add(errorCase);
        
    }
    */
    class PODException extends Exception{  }
}