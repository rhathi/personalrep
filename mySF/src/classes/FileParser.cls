public interface FileParser {
    List<Object> parseFile(Blob blobContent);

}