public class DocumentDeliveryParser implements FileParser {
    
    public static Sr[] srs {get; set;}


    public DocumentDeliveryParser() {
        srs = new Sr[] {};
    }

    public List<Object> parseFile(Blob blobContent) {
    String queryNameMod = 'Tim'+ '*';
List<List<sObject>> objects = [Find :queryNameMod
                                    IN Name Fields
                                    Returning Contact(Id, Name)];
            if (!objects.isEmpty()) {
                for (List<sObject> oList : objects) {
                    if (!oList.isEmpty()) {
                        for (sObject o : oList) {
                            if (o instanceof Contact) {
                                Contact temp = (Contact) o;
                                System.debug('rhathiii'+temp);
                                System.debug('rhathiii'+temp.Name);
                            }
                        }
                    }
                }
            }                                    
                                    
                                    
        try {
            String stringContent = blobContent.toString(); System.debug('length is'+stringContent.length());
            if (stringContent.contains('<?xml')) {
                while (stringContent.indexOf('<') != 0) {
                    stringContent = stringContent.substring(1);
                }
            }


            //processString(stringContent); 
                    
        } catch (XmlException e) {
               
        }
        return srs;
    }
    

    
    @future
    private static void processString(String subData){
        XmlStreamReader reader = new XmlStreamReader(subData);
        while (reader.hasNext()) { 
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
                if ('Sr' == reader.getLocalName()) {
                    parseSr(reader);
                }
            }
            reader.next();
        }
    }
    
    
    private static void parseSr(XmlStreamReader reader) {
        Sr v_sr = new Sr();
        while (reader.hasNext()) { 
            if (reader.getEventType() == XmlTag.END_ELEMENT) {
                if ('Sr' == reader.getLocalName()) {
                    break;
                }
            } else if (reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() != 'Sr') {
                String localName = reader.getLocalName();
                String charValue;
                reader.next();
                if (reader.getEventType() == XmlTag.CHARACTERS) {
                    charValue = reader.getText();
                }
                if ('UID' == localName) {
                    v_sr.uid = charValue;
                } else if ('ExpiryDate' == localName) {
                    v_sr.expiryDate = charValue;
                } else if ('DOB' == localName) {
                    v_sr.dob = charValue;
                } else if ('Language' == localName) {
                    v_sr.language = charValue;
                } else if ('CEACBarcodeNumber' == localName) {
                    v_sr.cEACBarcodeNumber = charValue;
                } else if ('InDate' == localName) {
                    v_sr.inDate = charValue;
                } else if ('InTime' == localName) {
                    v_sr.inTime = charValue;
                } else if ('Type' == localName) {
                    v_sr.v_type = charValue;
                } else if ('Address1' == localName) {
                    v_sr.address1 = charValue;
                } else if ('Address2' == localName) {
                    v_sr.address2 = charValue;
                } else if ('HAL' == localName) {
                    v_sr.hal = charValue;
                } else if ('State' == localName) {
                    v_sr.state = charValue;
                } else if ('City' == localName) {
                    v_sr.city = charValue;
                } else if ('HomePhone' == localName) {
                    v_sr.homePhone = charValue;
                } else if ('OfficePhone' == localName) {
                    v_sr.officePhone = charValue;
                } else if ('MobilePhone' == localName) {
                    v_sr.mobilePhone = charValue;
                } else if ('Zipcode' == localName) {
                    v_sr.zipcode = charValue;
                } else if ('Barcode' == localName) {
                    v_sr.barcode = charValue;
                }
            }
            reader.next();
        }
        srs.add(v_sr);
    }
   
    public class Sr {
            public String uid {get; set;}
            public String expiryDate {get; set;}
            public String dob {get; set;}
            public String language {get; set;}
            public String cEACBarcodeNumber {get; set;}
            public String inDate {get; set;}
            public String inTime {get; set;}
            public String v_type {get; set;}
            public String address1 {get; set;}
            public String address2 {get; set;}
            public String hal {get; set;}
            public String state {get; set;}
            public String city {get; set;}
            public String homePhone {get; set;}
            public String officePhone {get; set;}
            public String mobilePhone {get; set;}
            public String zipcode {get; set;}
            public String barcode {get; set;}
            
            public Sr() {}
    }
    
}