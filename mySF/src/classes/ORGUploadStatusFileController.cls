public class ORGUploadStatusFileController {  
        public Blob fileContent {get; set;}
        public String fileName {get; set;}
        public String fileType {get; set;}
        public String fileExtn {get; set;} 
        public String stringContent {get; set;}
        public List<Object> extractedData {get; set;}
        public FileParser parser {get; set;}
        public Boolean showConfirmation {get; set;}
        public String successMessage{get; set;}
        public Boolean hasFileExtnErrorMessage {get; set;}
        
        public ORGUploadStatusFileController() {
            
        }
        
        public List<SelectOption> getFileTypeMenuItems() {
            List<SelectOption> options = new List<SelectOption> ();
            options.add(new SelectOption('Document Delivery', 'Document Delivery'));
            options.add(new SelectOption('Citibank Transaction', 'Citibank Transaction'));
            options.add(new SelectOption('SCB Transaction', 'SCB Transaction'));
            options.add(new SelectOption('POD Transaction', 'POD Transaction'));
            return options;
        }
        
        public void uploadFile() {
            stringContent = fileContent.toString();
            fileExtn = fileName.substring(fileName.indexOf('.')+1,fileName.length());
            
            directToParser();
        }
        
        private void directToParser() {  
            if (fileType == 'Document Delivery') { 
                parser = new DocumentDeliveryParser();
            } else if (fileType == 'Citibank Transaction') {
               // parser = new TransactionParser();
            }else if (fileType == 'SCB Transaction') {
                //parser = new SCBTransactionParser();
            }
            
            extractedData = parser.parseFile(fileContent);
            System.debug('in UploadStatusFileController');
            showConfirmation = true;
            
            
        }
        
        static testmethod void testFileTypeMenuItems() {
            ORGUploadStatusFileController controller = new ORGUploadStatusFileController();
            List<SelectOption> options = controller.getFileTypeMenuItems();
            System.assert(options.get(0).getLabel() == 'Document Delivery');
            System.assert(options.get(0).getValue() == 'Document Delivery');
            System.assert(options.get(1).getLabel() == 'Citibank Transaction');
            System.assert(options.get(1).getValue() == 'Citibank Transaction');
            System.assert(options.get(2).getLabel() == 'SCB Transaction');
            System.assert(options.get(2).getValue() == 'SCB Transaction');            
            System.assert(options.get(3).getLabel() == 'POD Transaction');
            System.assert(options.get(3).getValue() == 'POD Transaction');
        }
        
        
                            
}